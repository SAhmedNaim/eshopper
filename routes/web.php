<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front-end---------------------------------------------------------------
Route::get('/', 'HomeController@index');

// products by category----------------------------------------------------
Route::get('category/{id}', 'HomeController@productsByCategory');
Route::get('manufacture/{id}', 'HomeController@productsByManufacture');
Route::get('product/{id}', 'HomeController@productDetails');

// Cart--------------------------------------------------------------------
Route::post('add-to-cart', 'CartController@index');
Route::get('show-cart', 'CartController@showCart');
Route::get('delete-cart/{id}', 'CartController@deleteCart');
Route::post('update-cart', 'CartController@updateCart');

// Back-end----------------------------------------------------------------
Route::get('/admin', 'AdminController@index');
Route::get('/dashboard', 'AdminController@showDashboard');
Route::post('/admin-dashboard', 'AdminController@dashboard');
Route::get('/logout', 'SuperAdminController@logout');
Route::get('manage-order', 'CheckoutController@manageOrder');
Route::get('edit-order/{id}', 'CheckoutController@editOrder');
Route::get('payment-paid/{id}', 'CheckoutController@paymentPaid');

// Category----------------------------------------------------------------
Route::get('/add-category', 'CategoryController@index');
Route::get('all-category', 'CategoryController@all_category');
Route::post('save-category', 'CategoryController@save_category');
Route::get('inactive-category/{id}', 'CategoryController@inactiveCategory');
Route::get('active-category/{id}', 'CategoryController@activeCategory');
Route::get('edit-category/{id}', 'CategoryController@editCategory');
Route::post('update-category/{id}', 'CategoryController@updateCategory');
Route::get('delete-category/{id}', 'CategoryController@deleteCategory');


// Manufacture-------------------------------------------------------------
Route::get('add-manufacture', 'ManufactureController@index');
Route::get('all-manufacture', 'ManufactureController@allManufacture');
Route::post('save-manufacture', 'ManufactureController@saveManufacture');
Route::get('inactive-manufacture/{id}', 'ManufactureController@inactiveManufacture');
Route::get('active-manufacture/{id}', 'ManufactureController@activeManufacture');

Route::get('edit-manufacture/{id}', 'ManufactureController@editManufacture');
Route::post('update-manufacture/{id}', 'ManufactureController@updateManufacture');
Route::get('delete-manufacture/{id}', 'ManufactureController@deleteManufacture');


// Product-----------------------------------------------------------------
Route::get('add-product', 'ProductController@index');
Route::post('save-product', 'ProductController@saveProduct');
Route::get('all-products', 'ProductController@allProducts');
Route::get('inactive-product/{id}', 'ProductController@inactiveProduct');
Route::get('active-product/{id}', 'ProductController@activeProduct');
Route::get('delete-product/{id}', 'ProductController@deleteProduct');

// Slider------------------------------------------------------------------
Route::get('add-slider', 'SliderController@addSlider');
Route::post('save-slider', 'SliderController@saveSlider');
Route::get('all-slider', 'SliderController@allSlider');

Route::get('inactive-slider/{id}', 'SliderController@inactiveSlider');
Route::get('active-slider/{id}', 'SliderController@activeSlider');
Route::get('delete-slider/{id}', 'SliderController@deleteSlider');

// Checkout----------------------------------------------------------------
Route::get('login-checkout', 'CheckoutController@loginCheckout');
Route::get('checkout', 'CheckoutController@checkout');

// Customer Register-------------------------------------------------------
Route::post('customer-register', 'CheckoutController@customerRegister');
Route::post('customer-login', 'CheckoutController@customerLogin');
Route::get('logout', 'CheckoutController@logout');

// Shipping----------------------------------------------------------------
Route::post('save-shipping', 'CheckoutController@saveShipping');
Route::get('payment', 'CheckoutController@payment');
Route::post('payment-method', 'CheckoutController@paymentMethod');
Route::get('congratulation', 'CheckoutController@congratulation');

