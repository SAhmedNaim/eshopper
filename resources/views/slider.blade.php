@section('slider')

@php
    $data = DB::table('sliders')
            ->where('publication_status', 1)
            ->get();

    $total = count($data);
@endphp

<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                        @for($i = 0; $i < $total; $i++)

                        <li data-target="#slider-carousel" data-slide-to="{{ $i }}" 

                        @if($i == 0)

                        class="active"

                        @endif

                        ></li>

                        @endfor

                    </ol>
                    
                    <div class="carousel-inner">

                        @php $i = 0; @endphp

                        @foreach($data as $slider)

                        <div class="item @if($i == 0) active @endif">

                            <div class="col-sm-12">
                                <img src="{{ URL::to($slider->image) }}" class="img-responsive" width="950px" style="height: 350px !important;" alt="Missing" />
                            </div>


                            <!-- <div class="col-sm-6">
                                <h1><span>E</span>-SHOPPER</h1>
                                <h2>Free E-Commerce Template</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <button type="button" class="btn btn-default get">Get it now</button>
                            </div>
                            <div class="col-sm-6">
                                <img src="{{ URL::to('frontend/images/home/girl1.jpg') }}" class="girl img-responsive" alt="" />
                                <img src="{{ URL::to('frontend/images/home/pricing.png') }}" class="pricing" alt="" />
                            </div> -->


                        </div>

                        @php $i++ @endphp

                        @endforeach

                    </div>
                    
                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
                
            </div>
        </div>
    </div>
</section><!--/slider-->

@endsection