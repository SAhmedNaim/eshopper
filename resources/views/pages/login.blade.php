@extends('layout')

@section('content')

    <div class="col-sm-3 col-sm-offset-1">
        <div class="login-form"><!--login form-->
            <h2>Login to your account</h2>
            <form action="{{ URL::to('customer-login') }}" method="POST">
                {{ csrf_field() }}
                <input type="email" name="email" placeholder="Email Address" />
                <input type="password" name="password" placeholder="Password" />
                <input type="submit" class="btn btn-default" value="Login"/>
            </form>
        </div><!--/login form-->
    </div>
    <div class="col-sm-1">
        <h2 class="or">OR</h2>
    </div>
    <div class="col-sm-3">
        <div class="signup-form"><!--sign up form-->
            <h2>New User Signup!</h2>
            <form action="{{ url('customer-register') }}" method="POST">
                {{ csrf_field() }}
                <input type="text" placeholder="Username" name="username"/>
                <input type="email" placeholder="Email Address" name="email"/>
                <input type="text" placeholder="Cell" name="cell"/>
                <input type="password" placeholder="Password" name="password"/>
                <input type="submit" name="submit" class="btn btn-default" value="Signup"/>
            </form>
        </div><!--/sign up form-->
    </div>
			
@endsection