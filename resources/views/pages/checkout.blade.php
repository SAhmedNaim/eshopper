@extends('layout')

@section('content')

    <style>
        .register-req {
            width: 840px;
            margin-left: 300px;
        }
    </style>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="register-req">
				<p>Fill up to proceed</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					
					<div class="col-sm-8 clearfix">
						<div class="bill-to">
							<p>Shipping details</p>
							<div class="form-one">
								<form action="{{ url('save-shipping') }}" method="POST">
									{{ csrf_field() }}
									<input type="text" name="email" placeholder="Email *">
									<input type="text" name="firstname" placeholder="First Name *">
									<input type="text" name="lastname" placeholder="Last Name *">
									<input type="text" name="address" placeholder="Address *">
									<input type="text" name="cell" placeholder="Cell">
									<input type="submit" name="submit" value="Done"/>
								</form>
							</div>
						</div>
					</div>
								
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->

	

@endsection