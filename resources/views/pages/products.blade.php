@extends('layout')

@section('content')
<div class="col-sm-9 padding-right">
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Products</h2>

        @foreach($data as $product)

        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                        <div class="productinfo text-center">
                            <img src="{{ asset('/'.$product->image) }}" alt="Missing" width="250px" height="230px" />
                            <h2>{{ $product->product_price }} BDT</h2>
                            <p>{{ $product->product_name }}</p>
                            <a href="{{ URL::to('product/'.$product->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>{{ $product->product_price }} BDT</h2>
                                <p>{{ $product->product_name }}</p>
                                <a href="{{ URL::to('product/'.$product->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                        </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="#"><i class="fa fa-plus-square"></i>
                        @if(isset($product->category_name))
                            {{ $product->category_name }}
                        @elseif(isset($product->manufacture_name))
                            {{ $product->manufacture_name }}
                        @endif
                        </a></li>
                        <li><a href="{{ URL::to('product/'.$product->id) }}"><i class="fa fa-plus-square"></i>Details</a></li>
                    </ul>
                </div>
            </div>
        </div>

        @endforeach

    </div><!--features_items-->
    
</div>
@endsection