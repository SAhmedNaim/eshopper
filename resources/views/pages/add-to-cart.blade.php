@extends('layout')

@section('content')

<style>
.cart_info {
	width: 850px !important;
}
</style>

<div class="col-sm-9">
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Image</td>
							<td class="description">Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>

					@foreach(Cart::content() as $value)

						<tr>
							<td class="cart_product">
								<a href=""><img src="{{ $value->options->image }}" width="100px" height="80px" alt="Missing"></a>
							</td>
							<td class="cart_description">
								<h4><a href="" style="margin-left:45px;">{{ $value->name  }}</a></h4>
							</td>
							<td class="cart_price">
								<p>{{ $value->price }}</p>
							</td>
							<td class="cart_quantity">
								<!-- <div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{ $value->qty }}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div> -->
								<div class="cart_quantity_button">
									
									<form action="{{ URL::to('update-cart') }}" method="POST">
										{{ csrf_field() }}
										<input class="cart_quantity_input" type="text" name="qty" value="{{ $value->qty }}" autocomplete="off" size="2">
										<input type="hidden" name="rowId" value="{{ $value->rowId }}">
										<input type="submit" value="Update" class="btn btn-sm btn-default">
									</form>

								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{ $value->total }}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" onclick="return confirm('Are you sure to remove?');" href="{{ URL::to('/delete-cart/'.$value->rowId) }}"><i class="fa fa-times"></i></a>
							</td>
						</tr>

					@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>{{ Cart::subtotal() }}</span></li>
							<li>Eco Tax <span>{{ Cart::tax() }}</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{ Cart::total() }}</span></li>
						</ul>

						@if(Session::get('id'))
							<a class="btn btn-default check_out" href="{{ URL::to('checkout') }}">Check Out</a>
						@else
							<a class="btn btn-default check_out" href="{{ URL::to('login-checkout') }}">Check Out</a>
						@endif

					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

</div>

@endsection