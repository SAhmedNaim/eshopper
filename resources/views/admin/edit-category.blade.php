@extends('admin-layout')

@section('admin-content')
			
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{ URL::to('dashboard') }}">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="#">Edit Category</a>
		</li>
	</ul>


			
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Category</h2>
			</div>
			<p class="alert-success">
				@php 

					$message = Session::get('message');

					if($message)
					{
						echo $message;
					}

					Session::put('message', NULL);

				@endphp

				@php
				foreach($data as $category)
				{
					$category_id 			= $category -> category_id;
					$category_name 			= $category -> category_name;
					$category_description	= $category -> category_description;
				}
				@endphp
				
			</p>
			<div class="box-content">
				<form class="form-horizontal" action="{{ url('update-category/'.$category_id) }}" method="POST">

					{{ csrf_field() }}

					<fieldset>

						<div class="control-group">
							<label class="control-label" for="category_name">Category Name</label>
						  	<div class="controls">
								<input type="text" class="input-xlarge" id="category_name" name="category_name" value="{{ $category_name }}" required>
						  	</div>
						</div>
	  
						<div class="control-group hidden-phone">
						  	<label class="control-label" for="category_description">Category Description</label>
						  	<div class="controls">
								<textarea class="cleditor" name="category_description" id="category_description" rows="3">{{ $category_description }}</textarea>
						  	</div>
						</div>

						<div class="form-actions">
						  	<button type="submit" class="btn btn-primary">Update</button>
						</div>

					</fieldset>
				</form>   

			</div>
		</div><!--/span-->

	</div><!--/row-->

@endsection