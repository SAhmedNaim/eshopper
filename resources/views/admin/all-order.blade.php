@extends('admin-layout')

@section('admin-content')

	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{ URL::to('dashboard') }}">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">All Order</a></li>
	</ul>

	@php 

	$message = Session::get('message'); 

	if($message)
	{
		echo '<p class="alert-success" style="padding: 15px;">';
			echo $message;
			Session::put('message', NULL);
		echo '</p>';
	}
	@endphp

	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
						  <th>Sl</th>
						  <th>Customer Name</th>
						  <th>Order Total</th>
						  <th>Order Status</th>
						  <th>Actions</th>
					  </tr>
				  </thead>   
				  <tbody>

				  	@php $i = 1; @endphp

				  	@foreach($data as $value)

					<tr>
						<td>{{ $i++ }}</td>
						<td class="center">{{ $value -> username }}</td>
						<td class="center">{{ $value -> order_total }}</td>
						<td class="center">
                            <span class="label label-success">{{ $value->order_status }}</span>
						</td>
						<td class="center">
							<a class="btn btn-success" href="{{ URL::to('payment-paid/'.$value->order_id) }}">
								<i class="halflings-icon white thumbs-up"></i>  
							</a>
							<a class="btn btn-info" href="{{ URL::to('edit-order/'.$value->order_id) }}">
								<i class="halflings-icon white edit"></i>  
							</a>
						</td>
					</tr>

					@endforeach


				  </tbody>
			  </table>            
			</div>
		</div><!--/span-->
	
	</div><!--/row-->


@endsection