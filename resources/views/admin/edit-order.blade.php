@extends('admin-layout')

@section('admin-content')

<div class="box span6">
    <div class="box-header">
        <h2><i class="halflings-icon align-justify"></i><span class="break"></span>Customer Details</h2>
        <div class="box-icon">
            <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Cell</th>                                   
                </tr>
            </thead>   
            <tbody>

            @foreach($data as $value)
                <tr>
                    <td>{{ $value->username }}</td>
                    <td class="center">{{ $value->cell }}</td>    
                </tr>
            @endforeach
            
            </tbody>
        </table>
    </div>
</div>

<div class="box span6">
    <div class="box-header">
        <h2><i class="halflings-icon align-justify"></i><span class="break"></span>Shipping Details</h2>
        <div class="box-icon">
            <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Address</th>
                    <th>Cell</th>
                    <th>Email</th>                                          
                </tr>
            </thead>   
            <tbody>
            @foreach($data as $value)
                <tr>
                    <td>{{ $value->firstname . ' ' . $value->lastname }}</td>
                    <td class="center">{{ $value->address }}</td>
                    <td class="center">{{ $value->cell }}</td>
                    <td class="center">{{ $value->email }}</td>
                </tr> 
            @endforeach                 
            </tbody>
        </table>
    </div>
</div>

<div class="box span11">
    <div class="box-header">
        <h2><i class="halflings-icon align-justify"></i><span class="break"></span>Order Details</h2>
        <div class="box-icon">
            <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Product Name</th>
                    <th>Product Price</th>
                    <th>Quantity</th>                                     
                    <th>Sub Total</th>                                     
                </tr>
            </thead>   
            <tbody>
            @php $i = 1; $total = 0; @endphp
            @foreach($data as $value)
            <tr>
                <td>{{ $i++ }}</td>
                <td class="center">{{ $value->product_name }}</td>
                <td class="center">{{ $value->product_price }}</td>
                <td class="center">{{ $value->product_quantity }}</td>
                <td class="center">{{ $value->product_price }}</td>
                @php $total = $total + $value->product_price; @endphp
            </tr>
            @endforeach    
            <tr>
                <td></td>
                <td class="center">Total</td>
                <td class="center"></td>
                <td class="center"></td>
                <td class="center">{{ $total }}</td>
            </tr>                
            </tbody>
        </table>
    </div>
</div>

@endsection