@extends('admin-layout')

@section('admin-content')

	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{ URL::to('dashboard') }}">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">All Slider</a></li>
	</ul>

	@php 

	$message = Session::get('message'); 

	if($message)
	{
		echo '<p class="alert-success" style="padding: 15px;">';
			echo $message;
			Session::put('message', NULL);
		echo '</p>';
	}
	@endphp

	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
						  <th>Sl</th>
						  <th>Image</th>
						  <th>Status</th>
						  <th>Actions</th>
					  </tr>
				  </thead>   
				  <tbody>

				  	@php $i = 1; @endphp

				  	@foreach($data as $slider)

					<tr>
						<td>{{ $i++ }}</td>
						<td class="center">
							<img src="{{ asset('/'.$slider->image) }}" alt="Missing" width="550px" height="100px">
						</td>
						<td class="center">

						@if($slider -> publication_status == 1)
							<span class="label label-success">
								{{ 'Active' }}
							</span>
						@else
							<span class="label label-danger">
								{{ 'Inactive' }}
							</span>
						@endif

						</td>
						<td class="center">

						@if($slider -> publication_status == 1)
							<a class="btn btn-warning" href="{{ URL::to('inactive-slider/'.$slider->id) }}">
								<i class="halflings-icon white thumbs-down"></i>  
							</a>
						@else
							<a class="btn btn-success" href="{{ URL::to('active-slider/'.$slider->id) }}">
								<i class="halflings-icon white thumbs-up"></i>  
							</a>
						@endif

							<a class="btn btn-info" href="{{ URL::to('edit-slider/'.$slider->id) }}">
								<i class="halflings-icon white edit"></i>  
							</a>
							<a class="btn btn-danger" href="{{ URL::to('delete-slider/'.$slider->id) }}" onclick="return confirm('Are you sure to delete?');">
								<i class="halflings-icon white trash"></i> 
							</a>
						</td>
					</tr>

					@endforeach


				  </tbody>
			  </table>            
			</div>
		</div><!--/span-->
	
	</div><!--/row-->


@endsection