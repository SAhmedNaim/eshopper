@extends('admin-layout')

@section('admin-content')
			
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{ URL::to('dashboard') }}">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="#">Edit Manufacture</a>
		</li>
	</ul>


			
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Manufacture</h2>
			</div>
			<p class="alert-success">
				@php 

					$message = Session::get('message');

					if($message)
					{
						echo $message;
					}

					Session::put('message', NULL);

				@endphp

			@php
			foreach($data as $manufacture)
			{
				$id 						= $manufacture -> id;
				$manufacture_name 			= $manufacture -> manufacture_name;
				$manufacture_description	= $manufacture -> manufacture_description;
			}
			@endphp
				
			</p>
			<div class="box-content">
				<form class="form-horizontal" action="{{ url('update-manufacture/'.$id) }}" method="POST">

					{{ csrf_field() }}

					<fieldset>

						<div class="control-group">
							<label class="control-label" for="category_name">Manufacture Name</label>
						  	<div class="controls">
								<input type="text" class="input-xlarge" id="manufacture_name" name="manufacture_name" value="{{ $manufacture_name }}" required>
						  	</div>
						</div>
	  
						<div class="control-group hidden-phone">
						  	<label class="control-label" for="category_description">Manufacture Description</label>
						  	<div class="controls">
								<textarea class="cleditor" name="manufacture_description" id="manufacture_description" rows="3">{{ $manufacture_description }}</textarea>
						  	</div>
						</div>

						<div class="form-actions">
						  	<button type="submit" class="btn btn-primary">Update</button>
						</div>

					</fieldset>
				</form>   

			</div>
		</div><!--/span-->

	</div><!--/row-->

@endsection