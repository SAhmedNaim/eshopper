@extends('admin-layout')

@section('admin-content')

@php
	$data['category'] 	= DB::table('tbl_category')
							->where('publication_status', 1)
							->get();

	$data['manufacture'] 	= DB::table('manufactures')
								->where('publication_status', 1)
								->get();
@endphp
			
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{ URL::to('dashboard') }}">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="#">Add Product</a>
		</li>
	</ul>


			
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Product</h2>
			</div>
			<p class="alert-success">
				@php 

					$message = Session::get('message');

					if($message)
					{
						echo $message;
					}

					Session::put('message', NULL);

				@endphp
			</p>
			<div class="box-content">
				<form class="form-horizontal" action="{{ url('save-product') }}" method="POST" enctype="multipart/form-data">

					{{ csrf_field() }}

				  	<fieldset>
						<div class="control-group">
							<label class="control-label" for="product_name">Product Name</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="product_name" name="product_name" placeholder="Enter Product Name" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="category_id">Product Category</label>
								<div class="controls">
									<select name="category_id" id="category_id">
										<option value="">Choose Category</option>

										@foreach($data['category'] as $category)
										
										<option value="{{ $category -> category_id }}">
											{{ $category -> category_name }}
										</option>

										@endforeach

									</select>
								</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="manufacture_id">Manufacture Name</label>
								<div class="controls">
									<select name="manufacture_id" id="manufacture_id">
										<option value="">Chosse Maufacture</option>

										@foreach($data['manufacture'] as $manufacture)

										<option value="{{ $manufacture -> id }}">
											{{ $manufacture -> manufacture_name }}
										</option>

										@endforeach

									</select>
								</div>
						</div>
  
						<div class="control-group hidden-phone">
							<label class="control-label" for="product_shortDescription">Product Short Description</label>
							<div class="controls">
								<textarea id="product_shortDescription" class="cleditor" name="product_shortDescription" id="category_description" rows="3"></textarea>
							</div>
						</div>

						<div class="control-group hidden-phone">
							<label class="control-label" for="product_longDescription">Product Long Description</label>
							<div class="controls">
								<textarea id="product_longDescription" class="cleditor" name="product_longDescription" id="category_description" rows="3"></textarea>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="product_price">Product Price</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="product_price" name="product_price" placeholder="Enter Product Price" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="image">Product Image</label>
							<div class="controls">
								<input type="file" class="input-xlarge" id="image" name="image" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="size">Product Size</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="size" name="size" placeholder="Enter Product Size" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="color">Product Color</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="color" name="color" placeholder="Enter Product Color" required>
							</div>
						</div>

						<div class="control-group hidden-phone">
							<label class="control-label" for="publication_status">Publication Status</label>
							<div class="controls">
								<input type="checkbox" name="publication_status" value="1">
							</div>
						</div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Save changes</button>
							<button type="reset" class="btn">Cancel</button>
						</div>
					</fieldset>
				</form>   

			</div>
		</div><!--/span-->

	</div><!--/row-->

@endsection