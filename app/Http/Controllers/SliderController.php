<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class SliderController extends Controller
{
    public function addSlider()
    {
    	return view('admin.add-slider');
    }

    public function saveSlider(Request $request)
    {
    	$data = array();

    	$data['publication_status'] = $request->publication_status;

    	$image = $request->file('image');

    	if($image)
    	{
    		$image_name 		= str_random(20);
    		$extension 			= strtolower($image->getClientOriginalExtension());
    		$image_full_name 	= $image_name . '.' . $extension;

    		$upload_path 		= 'slider/';
    		$image_url 			= $upload_path . $image_full_name;

    		$success 			= $image -> move($upload_path, $image_full_name);

    		if($success)
    		{
    			$data['image'] 	= $image_url;

    			DB::table('sliders')->insert($data);

    			Session::put('message', 'Slider Added Successfully!!!');
    		}

    	}
    	else
    	{
	    	$data['image'] = "";

	    	DB::table('sliders')->insert($data);

	    	Session::put('message', 'Slider Added Successfully without Image!!!');
    	}

    	return Redirect::to('add-slider');
    }

    public function allSlider()
    {
    	$data = DB::table('sliders')->get();

    	return view('admin.all-slider', compact('data'));
    }

    public function inactiveSlider($id)
    {
        DB::table('sliders')->where('id', $id)
                ->update([
                    'publication_status' => '0',
                ]);

        Session::put('message', 'Slider Inactivated Successfully');

        return Redirect('all-slider');
    }

    public function activeSlider($id)
    {
        DB::table('sliders')->where('id', $id)
                ->update([
                    'publication_status' => '1',
                ]);

        Session::put('message', 'Slider Activated Successfully');

        return Redirect('all-slider');
    }

    public function deleteSlider($id)
    {
        DB::table('sliders')->where('id', $id)->delete();

        Session::put('message', 'Slider Deleted Successfully!!!');

        return Redirect::to('all-slider');
    }

    
    	
    	
    

}
