<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class ProductController extends Controller
{
    public function index()
    {
    	return view('admin.add-product');
    }

    public function saveProduct(Request $request)
    {
    	$data = array();

    	$data['product_name'] 				= $request->product_name;
    	$data['category_id'] 				= $request->category_id;
    	$data['manufacture_id'] 			= $request->manufacture_id;
    	$data['product_shortDescription'] 	= $request->product_shortDescription;
    	$data['product_longDescription'] 	= $request->product_longDescription;
    	$data['product_price'] 				= $request->product_price;
    	$data['size'] 						= $request->size;
    	$data['color'] 						= $request->color;
    	$data['publication_status'] 		= $request->publication_status;

    	$image = $request->file('image');

    	if($image)
    	{
    		$image_name 		= str_random(20);
    		$extension 			= strtolower($image->getClientOriginalExtension());
    		$image_full_name 	= $image_name . '.' . $extension;

    		$upload_path 		= 'image/';
    		$image_url 			= $upload_path . $image_full_name;

    		$success 			= $image -> move($upload_path, $image_full_name);

    		if($success)
    		{
    			$data['image'] 	= $image_url;

    			DB::table('products')->insert($data);

    			Session::put('message', 'Product Added Successfully!!!');
    		}

    	}
    	else
    	{
	    	$data['image'] = "";

	    	DB::table('products')->insert($data);

	    	Session::put('message', 'Product Added Successfully without Image!!!');
    	}

    	return Redirect::to('add-product');
    }

    public function allProducts()
    {
        $data = DB::table('products')
                ->join('tbl_category', 'products.category_id', 'tbl_category.category_id')
                ->join('manufactures', 'products.manufacture_id', 'manufactures.id')
                ->select('products.*', 'tbl_category.category_name', 'manufactures.manufacture_name')
                ->get();

        return view('admin.all-product', compact('data'));
    }


    public function inactiveProduct($id)
    {
        DB::table('products')->where('id', $id)
                ->update([
                    'publication_status' => '0',
                ]);

        Session::put('message', 'Product Inactivated Successfully');

        return Redirect('all-products');
    }

    public function activeProduct($id)
    {
        DB::table('products')->where('id', $id)
                ->update([
                    'publication_status' => '1',
                ]);

        Session::put('message', 'Product Activated Successfully');

        return Redirect('all-products');
    }

    public function deleteProduct($id)
    {
        DB::table('products')->where('id', $id)->delete();

        Session::put('message', 'Product Deleted Successfully!!!');

        return Redirect::to('all-products');
    }

}
