<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use Illuminate\Support\Facades\Redirect;
// use Request;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $total_product = $request->total_product;
        $product_id = $request->product_id;

        $product = DB::table('products')->where('id', $product_id)->first();

        $data = array();
        $data['id']                 = $product->id;
        $data['qty']                = $total_product;
        $data['name']               = $product->product_name;
        $data['price']              = $product->product_price;
        $data['options']['image']   = $product->image;

        Cart::add($data);
        
        return Redirect::to('show-cart');
    }

    public function showCart()
    {
        $data = DB::table('tbl_category')->where('publication_status', 1)->get();

        return view('pages.add-to-cart', compact('data'));
    }

    public function deleteCart($id)
    {
        Cart::update($id,0);

        return Redirect::to('show-cart');
    }

    public function updateCart(Request $request)
    {
        $qty = $request->qty;
        $rowId = $request->rowId;

        Cart::update($rowId,$qty);

        return Redirect::to('show-cart');
    }
}
