<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class CategoryController extends Controller
{
    public function index()
    {
    	return view('admin.add-category');
    }

    public function all_category()
    {
    	$data = DB::table('tbl_category')->get();

    	return view('admin.all-category', compact('data'));
    }

    public function save_category(Request $request)
    {
    	$data = array();

    	$data['category_name'] 			= $request->category_name;
    	$data['category_description'] 	= $request->category_description;
    	$data['publication_status'] 	= $request->publication_status;

    	DB::table('tbl_category')->insert($data);

    	Session::put('message', 'Category Added Successfully');

    	return Redirect::to('add-category');
    }

    public function inactiveCategory($id)
    {
       	DB::table('tbl_category')->where('category_id', $id)
       			->update([
       				'publication_status' => '0',
       			]);

    	Session::put('message', 'Category Inactivated Successfully');

    	return Redirect('all-category');
    }

    public function activeCategory($id)
    {
    	DB::table('tbl_category')->where('category_id', $id)
       			->update([
       				'publication_status' => '1',
       			]);

    	Session::put('message', 'Category Activated Successfully');

    	return Redirect('all-category');
    }

    public function editCategory($id)
    {
    	$data = DB::table('tbl_category')->where('category_id', $id)->get();

    	return view('admin.edit-category', compact('data'));
    }

    public function updateCategory(Request $request, $id)
    {
    	$data = array();

    	$data['category_name'] 			= $request->category_name;
    	$data['category_description'] 	= $request->category_description;

    	DB::table('tbl_category')->where('category_id', $id)->update($data);

    	Session::put('message', 'Category Updated Successfully!!!');

    	return Redirect::to('all-category');
    }

    public function deleteCategory($id)
    {
    	DB::table('tbl_category')->where('category_id', $id)->delete();

    	Session::put('message', 'Category Deleted Successfully!!!');

    	return Redirect::to('all-category');
	}

}
