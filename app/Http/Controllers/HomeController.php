<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index()
    {
    	$data = DB::table('products')
                ->join('tbl_category', 'products.category_id', 'tbl_category.category_id')
                ->join('manufactures', 'products.manufacture_id', 'manufactures.id')
                ->select('products.*', 'tbl_category.category_name', 'manufactures.manufacture_name')
                ->where('products.publication_status', 1)
                ->limit(6)
                ->get();

    	return view('pages.home-content', compact('data'));
    }

    public function productsByCategory($id)
    {
        $data = DB::table('products')
                ->join('tbl_category', 'products.category_id', 'tbl_category.category_id')
                ->where('products.category_id', $id)
                ->where('products.publication_status', 1)
                ->limit(18)
                ->get();

        return view('pages.products', compact('data'));
    }

    public function productsByManufacture($id)
    {
        $data = DB::table('products')
                ->join('manufactures', 'products.manufacture_id', 'manufactures.id')
                ->where('products.manufacture_id', $id)
                ->where('products.publication_status', 1)
                ->limit(18)
                ->get();

        return view('pages.products', compact('data'));
    }

    public function productDetails($id)
    {
        $data = DB::table('products')
                ->join('tbl_category', 'products.category_id', 'tbl_category.category_id')
                ->join('manufactures', 'products.manufacture_id', 'manufactures.id')
                ->select('products.*', 'tbl_category.category_name', 'manufactures.manufacture_name')
                ->where('products.id', $id)
                ->where('products.publication_status', 1)
                ->get();

                // return $data;

        return view('pages.product-details', compact('data'));
    }

}
