<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use DB;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    public function loginCheckout()
    {
        return view('pages.login');
    }

    public function customerRegister(Request $request)
    {
        $customer = new Customer;
        $customer->username = $request->username;
        $customer->email    = $request->email;
        $customer->cell     = $request->cell;
        $customer->password = $request->password;
        $customer->save();

        $id = $customer->id;


        // N.B: As i am using elequent so i can fetch last inserted customer id by this way.
        // But while using query builder then, i should follow like below.
        // That's all about this mechanism
        /*
        $data = array();
        $data['username']   = $request->username;
        $data['email']      = $request->email;
        $data['cell']       = $request->cell;
        $data['password']   = $request->password;
        $id = Customer::insertGetId($data);
        */

        Session::put('id', $id);
        Session::put('username', $customer->username);

        return Redirect::to('checkout');
    }

    public function checkout()
    {
        return view('pages.checkout');
    }

    public function saveShipping(Request $request)
    {
        $data = array();

        $data['email'] = $request->email;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['address'] = $request->address;
        $data['cell'] = $request->cell;

        DB::table('shipping')->insert($data);

        $id = DB::table('shipping')->insertGetId($data);

        Session::put('shipping_id', $id);

        return Redirect::to('payment');
    }

    public function customerLogin(Request $request)
    {
        
        $email      = $request->email;
        $password   = $request->password;

        $result = DB::table('customers')
                    ->where('email', $email)
                    ->where('password', $password)
                    ->first();

        if($result)
        {
            Session::put('id', $result->id);

            return Redirect::to('checkout');
        }
        else
        {
            return Redirect::to('checkout-login');
        }
    }

    public function logout()
    {
        Session::flush();

        return Redirect::to('/');
    }

    public function payment()
    {
        return view('pages.payment');
    }

    public function paymentMethod(Request $request)
    {
        $payment = array();
        $payment['payment_method'] = $request['payment_method'];
        $payment['payment_status'] = 'pending';
        $payment_id = DB::table('payments')->insertGetId($payment);

        $order = array();
        $order['customer_id']   = Session::get('id');
        $order['shipping_id']   = Session::get('shipping_id');
        $order['payment_id']    = $payment_id;
        $order['order_total']   = Cart::total();
        $order['order_status']  = 'pending';
        $order_id = DB::table('orders')->insertGetId($order);

        $contents = Cart::content();

        $order_details = array();

        foreach($contents as $value)
        {
            $order_details['order_id'] = $order_id;
            $order_details['product_id'] = $value->id;
            $order_details['product_name'] = $value->name;
            $order_details['product_price'] = $value->price;
            $order_details['product_quantity'] = $value->qty;

            DB::table('order_details')->insert($order_details);
        }

        $message['info'] = 'Payment received by: ' . $payment['payment_method'];

        Cart::destroy();

        $message['url'] = 'congratulation';

        return $message;
    }

    public function congratulation()
    {
        return view('pages.congratulation');
    }

    // Admin panel part
    public function manageOrder()
    {
        $data = DB::table('orders')
                ->join('customers', 'orders.customer_id', 'customers.id')
                ->addSelect('orders.id as order_id', 'customers.*', 'orders.*')
                ->get();

        return view('admin.all-order', compact('data'));
    }

    public function editOrder($id)
    {
        // orders, customers, order_details, shipping

        // $data = DB::table('orders')
        //         ->join('customers', 'orders.customer_id', 'customers.id')
        //         ->join('order_details', 'order_details.order_id', 'orders.id')
        //         ->join('shipping', 'orders.shipping_id', 'shipping.id')
        //         ->where('orders.id', $id)
        //         ->select('orders.*', 'order_details.*', 'shipping.*', 'customers.*')
        //         ->get();

        $data = DB::table('orders')
                ->join('customers', 'orders.customer_id', 'customers.id')
                ->join('shipping', 'orders.shipping_id', 'shipping.id')
                ->join('order_details', 'order_details.order_id', 'orders.id')
                ->where('orders.id', $id)
                ->get();

                // return $data;

        return view('admin.edit-order', compact('data'));
    }

    public function paymentPaid($id)
    {
        $data = DB::table('orders')
                ->join('payments', 'orders.payment_id', 'payments.id')
                ->get();

        foreach($data as $value)
        {
            $payment_id = $value->payment_id;
        }
        
        DB::table('payments')
        ->where('id', $payment_id)
        ->update([
            'payment_status' => 'Paid'
        ]);

        return Redirect::to('dashboard');
    }

}
