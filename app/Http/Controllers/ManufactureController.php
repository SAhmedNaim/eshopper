<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class ManufactureController extends Controller
{
    public function index()
    {
    	return view('admin.add-manufacture');
    }

	public function saveManufacture(Request $request)
    {
    	$data = array();

    	$data['manufacture_name'] 			= $request->manufacture_name;
    	$data['manufacture_description'] 	= $request->manufacture_description;
    	$data['publication_status'] 		= $request->publication_status;

    	DB::table('manufactures')->insert($data);

    	Session::put('message', 'Manufacture Added Successfully');

    	return Redirect::to('add-manufacture');
    }

    public function allManufacture()
    {
    	$data = DB::table('manufactures')->get();

    	return view('admin.all-manufacture', compact('data'));
    }

    public function inactiveManufacture($id)
    {
       	DB::table('manufactures')->where('id', $id)
       			->update([
       				'publication_status' => '0',
       			]);

    	Session::put('message', 'Manufacture Inactivated Successfully');

    	return Redirect('all-manufacture');
    }

    public function activeManufacture($id)
    {
    	DB::table('manufactures')->where('id', $id)
       			->update([
       				'publication_status' => '1',
       			]);

    	Session::put('message', 'Manufacture Activated Successfully');

    	return Redirect('all-manufacture');
    }

    public function editManufacture($id)
    {
    	$data = DB::table('manufactures')->where('id', $id)->get();

    	return view('admin.edit-manufacture', compact('data'));
    }

    public function updateManufacture(Request $request, $id)
    {
    	$data = array();

    	$data['manufacture_name'] 			= $request->manufacture_name;
    	$data['manufacture_description'] 	= $request->manufacture_description;

    	DB::table('manufactures')->where('id', $id)->update($data);

    	Session::put('message', 'Manufacture Updated Successfully!!!');

    	return Redirect::to('all-manufacture');
    }

    public function deleteManufacture($id)
    {
    	DB::table('manufactures')->where('id', $id)->delete();

    	Session::put('message', 'Manufacture Deleted Successfully!!!');

    	return Redirect::to('all-manufacture');
	}



}
